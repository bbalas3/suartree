import os
import pickle
import random
import pandas as pd
from osgeo import ogr

from src import rect
from src import rtree
from src.rect import Node

__author__ = "Booma Sowkarthiga Balasubramani"


def generate_random(bound_a, bound_b):
    """
    Pick a random value of x and y to generate a query region

    :param bound_a: lower bound
    :param bound_b: upper bound
    :return:
    """
    return random.uniform(bound_a, bound_b)


def area(x, y, xx, yy):
    """
    Area of a region

    :param x: x of MBR in root
    :param y: y of MBR in root
    :param xx: xx of MBR in root
    :param yy: yy of MBR in root
    :return:
    """
    w = xx - x
    h = yy - y
    return w * h


def calculate_metrics(total_experiments, total_metric):
    """
    Average out the metrics based on the number of experiments

    :param total_experiments: total number of experiments ran
    :param total_metric: sum of the metric for all the experiments
    :return:
    """
    avg_metric = total_metric / total_experiments
    return avg_metric


def query_sequential_intersection(data_object, query_bounds):
    """
    Check for intersection of polygons in the csv file sequentially

    :param obj: a data object
    :param query_bounds: bounds of the query region
    :return:
    """
    counter = 0
    wkt1 = query_bounds
    poly1 = ogr.CreateGeometryFromWkt(wkt1)
    for index, row in data_object.iterrows():
        wkt2 = row['the_geom_poly_lu']

        poly2 = ogr.CreateGeometryFromWkt(wkt2)

        intersection = poly1.Intersection(poly2)
        if intersection is not None and row['landuse'] == 1130:
            counter += 1

    return counter


def query_rtree(root, rect):
    """
    Query R-tree

    :param root: Root node object of the R-tree
    :param rect: Query region
    :return:
    """
    root.cursor._become(0)
    root_idx = root.cursor.index
    stack = [root_idx]
    results = []
    estimated_node_access_count = 0
    actual_node_access_count = 0
    DONE = []
    while stack:
        cur = stack[0]
        del stack[0]
        root.cursor._become(cur)
        if not root.cursor.has_children():
            if root.cursor.is_leaf():
                estimated_node_access_count += 1
                results.append(root.cursor.info.land_use)
            continue

        # If query region intersects with the node, traverse the sub-tree, and then check the siblings
        if rect.does_intersect(root.cursor.rect):
            estimated_node_access_count += 1
            for c in root.cursor.children():
                if c.index in DONE:
                    continue
                DONE.append(c.index)
                stack.append(c.index)
    return results, estimated_node_access_count, actual_node_access_count


def parse_data(data_set, fields):
    """
    Parses the input data set based on the fields passed

    :param data_set: a CSV file
    :param fields: attributes in the CSV file
    :return:
    """
    data_obj = pd.read_csv(data_set, sep=',',
                           header=None,
                           names=fields)
    #                  sep=',', header=0)

    data_obj.dropna(inplace=True)
    data_obj.bextent = data_obj.bextent.astype('object')
    data_obj.the_geom_poly_lu = data_obj.the_geom_poly_lu.astype('object')
    data_obj.GeoId = data_obj.GeoId.astype('int64')
    data_obj.tract_bloc = data_obj.tract_bloc.astype('int64')
    data_obj.total_pop = data_obj.total_pop.astype('int64')
    data_obj.landuse = data_obj.landuse.astype('int64')
    return data_obj


def load(data_set, fields):
    """
    Load data set and build R-tree and SUAR-tree

    :param data_set: a CSV file
    :return:
    """
    pickle_file_Node = "NodePickle_{}".format(data_set.replace('/', '_').replace('.', '_'))
    pickle_file_RTree = "NodePickleR_{}".format(data_set.replace('/', '_').replace('.', '_'))
    leaf_obj = parse_data(data_set, fields)
    if not os.path.exists(pickle_file_Node) and not os.path.exists(pickle_file_RTree):
        t = rtree.RTree()
        for index, row in leaf_obj.iterrows():
            s = row['bextent']
            bb_coords = s.split()
            x = float(bb_coords[0].split("POLYGON((", 1)[1])
            y = float(bb_coords[1].split(",")[0])
            xx = float(bb_coords[2].split(",")[1])
            xy = float(bb_coords[3].split(",")[0])

            desc1 = row['landuse']
            desc2 = row['lu_area']
            desc3 = row['pop_per_polygon']
            t.insert(index, rect.Descriptor(desc1, desc2, desc3), rect.Rect(x, y, xx, xy))

        # Create saur tree using RTree instance
        Node.backfill_descriptors(t)

        pickled_file_Node = open(pickle_file_Node, 'wb')
        pickled_file_RTree = open(pickle_file_RTree, 'wb')

        pickle.dump(Node.meta_data, pickle_file_Node)
        pickle.dump(t, pickle_file_RTree)

        pickled_file_Node.close()
        pickled_file_RTree.close()
    else:
        picklefile_Node = open(pickle_file_Node, 'rb')
        picklefile_RTree = open(pickle_file_RTree, 'rb')

        Node.meta_data = pickle.load(picklefile_Node)
        t = pickle.load(picklefile_RTree)

        picklefile_Node.close()
        picklefile_RTree.close()

    return {
        "SAUR_TREE": Node.meta_data,
        "RTREE": t
    }
