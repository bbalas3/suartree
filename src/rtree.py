from __future__ import absolute_import
from functools import total_ordering
import math, random, sys
import time
import array
from src.rect import Rect, union_all, NullRect, Descriptor

MAXCHILDREN = 10
MAX_KMEANS = 5


@total_ordering
class findMax(object):
    """ Find maximum value """

    def __init__(self, value):
        self.value = value

    def __cmpkey(self):
        return (self.value)

    def __hash__(self):
        return hash(self.__cmpkey())

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.value)

    def __eq__(self, other):
        return self.value == other.value

    def __lt__(self, other):
        return self.value < other.value

    def __le__(self, other):
        return self.value <= other.value

    def __gt__(self, other):
        print("Other: {} | Self: {}".format(other.value, self.value))
        return self.value > other.value

    def __ge__(self, other):
        return self.value >= other.value

    def __ne__(self, other):
        return self.value != other.value


class RTree(object):
    def __init__(self):
        self.count = 0
        self.stats = {
            "overflow_f": 0,
            "avg_overflow_t_f": 0.0,
            "longest_overflow": 0.0,
            "longest_kmeans": 0.0,
            "sum_kmeans_iter_f": 0,
            "count_kmeans_iter_f": 0,
            "avg_kmeans_iter_f": 0.0
        }

        # Not using objects directly as they take up too much memory
        # Instead, it uses pools of arrays:
        self.count = 0
        self.leaf_count = 0
        self.rect_pool = array.array('d')
        self.node_pool = array.array('L')
        self.info_pool = array.array('d')
        self.leaf_pool = []  # Leaf objects
        self.cursor = _NodeCursor.create(self, Descriptor(0, 0, 0), NullRect)

    def _ensure_pool(self, idx):
        if len(self.rect_pool) < (4 * idx):
            self.rect_pool.extend([0, 0, 0, 0] * idx)
            self.node_pool.extend([0, 0] * idx)
            self.info_pool.extend([0, 0, 0] * idx)

    def insert(self, o, oinfo, orect):
        self.cursor.insert(o, oinfo, orect)
        assert (self.cursor.index == 0)

    def query_rect(self, r):
        for x in self.cursor.query_rect(r): yield x

    def query_rect_condition(self, r, cond):
        for x in self.cursor.query_rect_condition(r, cond): yield x

    def query_point(self, p):
        for x in self.cursor.query_point(p): yield x

    def walk(self, pred):
        return self.cursor.walk(pred)


class _NodeCursor(object):
    @classmethod
    def create(cls, rooto, info, rect):
        idx = rooto.count
        rooto.count += 1
        rooto._ensure_pool(idx + 1)

        retv = _NodeCursor(rooto, idx, info, rect, 0, 0)

        retv._save_back()
        return retv

    @classmethod
    def create_with_children(cls, children, rooto):
        rect = union_all([c for c in children])
        nr = Rect(rect.x, rect.y, rect.xx, rect.yy)

        min_list = [c.info.land_use for c in children]
        min_min_list = min(min_list)
        max_list = [c.info.area for c in children]
        max_max_list = max(max_list)

        avg_list = [c.info.pop for c in children]
        avg_avg_list = sum(avg_list) / len(avg_list)
        assert (not rect.swapped_x)
        nc = _NodeCursor.create(rooto, Descriptor(min_min_list, max_max_list, avg_avg_list), rect)
        nc._set_children(children)
        assert (not nc.is_leaf())
        return nc

    @classmethod
    def create_leaf(cls, rooto, leaf_obj, leaf_info, leaf_rect):
        rect = Rect(leaf_rect.x, leaf_rect.y, leaf_rect.xx, leaf_rect.yy)
        rect.swapped_x = True  # Mark as leaf by setting the xswap flag

        res = _NodeCursor.create(rooto, leaf_info, rect)
        idx = res.index
        res.first_child = rooto.leaf_count
        rooto.leaf_count += 1
        res.next_sibling = 0
        res.info = leaf_info
        rooto.leaf_pool.append(leaf_obj)

        res._save_back()
        res._become(idx)
        assert (res.is_leaf())
        return res

    __slots__ = ("root", "npool", "rpool", "infopool", "index", "info", "rect", "next_sibling", "first_child")

    def __init__(self, rooto, index, info, rect, first_child, next_sibling):
        self.root = rooto
        self.rpool = rooto.rect_pool
        self.npool = rooto.node_pool
        self.infopool = rooto.info_pool

        self.index = index
        self.info = info
        self.rect = rect
        self.next_sibling = next_sibling
        self.first_child = first_child

    def walk(self, predicate):
        if (predicate(self, self.leaf_obj())):
            yield self

            if not self.is_leaf():
                for c in self.children():
                    for cr in c.walk(predicate):
                        yield cr

    def query_rect(self, r):
        """ Return things that intersect with 'r' """

        def p(o, x): return r.does_intersect(o.rect)

        for rr in self.walk(p):
            yield rr

    def walk_condition(self, predicate, cond):
        if predicate(self, self.leaf_obj()):
            yield self
            if not self.is_leaf():
                if (self.info.max_val - self.info.min_val) == 0 or (self.info.max_val - self.info.min_val) > cond:
                    for c in self.children():
                        for cr in c.walk_condition(predicate, cond):
                            yield cr

    def query_rect_condition(self, r, cond):
        """ Return things that intersect with 'r' """

        def p(o, x): return r.does_intersect(o.rect)

        for rr in self.walk_condition(p, cond):
            yield rr

    def query_point(self, point):
        """ Query by a point """

        def p(o, x): return o.rect.does_containpoint(point)

        for rr in self.walk(p):
            yield rr

    def lift(self):
        return _NodeCursor(self.root,
                           self.index,
                           self.info,
                           self.rect,
                           self.first_child,
                           self.next_sibling)

    def _become(self, index):
        recti = index * 4
        nodei = index * 2
        infoi = index * 3

        rp = self.rpool
        x = rp[recti]
        y = rp[recti + 1]
        xx = rp[recti + 2]
        yy = rp[recti + 3]

        if (x == 0.0 and y == 0.0 and xx == 0.0 and yy == 0.0):
            self.rect = NullRect
        else:
            self.rect = Rect(x, y, xx, yy)

        min_val = self.infopool[infoi]
        max_val = self.infopool[infoi + 1]
        avg_val = self.infopool[infoi + 2]

        self.info = Descriptor(min_val, max_val, avg_val)

        self.next_sibling = self.npool[nodei]
        self.first_child = self.npool[nodei + 1]

        self.index = index

    def is_leaf(self):
        return self.rect.swapped_x

    def has_children(self):
        return not self.is_leaf() and 0 != self.first_child

    def holds_leaves(self):
        if 0 == self.first_child:
            return True
        else:
            return self.has_children() and self.get_first_child().is_leaf()

    def get_first_child(self):
        fc = self.first_child
        c = _NodeCursor(self.root, 0, '', NullRect, 0, 0)
        c._become(self.first_child)
        return c

    def leaf_obj(self):
        if self.is_leaf():
            return self.root.leaf_pool[self.first_child]
        else:
            return None

    def _save_back(self):
        rp = self.rpool
        recti = self.index * 4
        nodei = self.index * 2
        infoi = self.index * 3

        if self.rect.x == 0 and self.rect.y == 0 and self.rect.xx == 0 and self.rect.yy == 0:
            rp[recti] = 0
            rp[recti + 1] = 0
            rp[recti + 2] = 0
            rp[recti + 3] = 0
        else:
            self.rect.write_raw_coords(rp, recti)

        self.infopool[infoi] = self.info.land_use
        self.infopool[infoi + 1] = self.info.area
        # Avg val set to int using math.ceil to resolve TypeError: array item must be integer
        self.infopool[infoi + 2] = math.ceil(self.info.pop)

        self.npool[nodei] = self.next_sibling
        self.npool[nodei + 1] = self.first_child

    def nchildren(self):
        i = self.index
        c = 0
        for x in self.children(): c += 1
        return c

    def insert(self, leafo, leafinfo, leafrect):
        index = self.index

        # tail recursion, made into loop
        while True:
            if self.holds_leaves():
                self.rect = self.rect.union(leafrect)

                self.info = leafinfo

                new_child = _NodeCursor.create_leaf(self.root, leafo, leafinfo, leafrect)

                self._insert_child(new_child)

                self._balance()

                # done: become the original again
                self._become(index)
                return
            else:
                # Not holding leaves, move down a level in the tree:
                # Micro-optimization:
                # inlining union() calls -- logic is:
                # ignored,child = min([ ((c.rect.union(leafrect)).area() - c.rect.area(),c.index) for c in self.children() ])
                child = None
                minarea = -1.0
                for c in self.children():
                    x, y, xx, yy = c.rect.coords()
                    lx, ly, lxx, lyy = leafrect.coords()
                    nx = x if x < lx else lx
                    nxx = xx if xx > lxx else lxx
                    ny = y if y < ly else ly
                    nyy = yy if yy > lyy else lyy
                    a = (nxx - nx) * (nyy - ny)
                    if minarea < 0 or a < minarea:
                        minarea = a
                        child = c.index

                self.rect = self.rect.union(leafrect)
                self._save_back()
                self._become(child)  # recurse.

    def _balance(self):
        if (self.nchildren() <= MAXCHILDREN):
            return

        t = time.clock()
        cur_score = -10
        s_children = [c.lift() for c in self.children()]
        memo = {}
        clusterings = [k_means_cluster(self.root, k, s_children) for k in range(2, MAX_KMEANS)]

        test = [(silhouette_coeff(c, memo), c) for c in clusterings]
        score, bestcluster = max(test, key=lambda x: x[0])
        nodes = [_NodeCursor.create_with_children(c, self.root) for c in bestcluster if len(c) > 0]
        self._set_children(nodes)
        dur = (time.clock() - t)
        c = float(self.root.stats["overflow_f"])
        oa = self.root.stats["avg_overflow_t_f"]
        self.root.stats["avg_overflow_t_f"] = (dur / (c + 1.0)) + (c * oa / (c + 1.0))
        self.root.stats["overflow_f"] += 1
        self.root.stats["longest_overflow"] = max(self.root.stats["longest_overflow"], dur)

    def _set_children(self, cs):
        self.first_child = 0

        if 0 == len(cs):
            return

        pred = None
        for c in cs:
            if pred is not None:
                pred.next_sibling = c.index
                pred._save_back()
            if 0 == self.first_child:
                self.first_child = c.index
            pred = c
        pred.next_sibling = 0
        pred._save_back()
        self._save_back()

    def _insert_child(self, c):
        c.next_sibling = self.first_child
        self.first_child = c.index
        c._save_back()
        self._save_back()

    def children(self):
        if (0 == self.first_child): return

        idx = self.index
        fc = self.first_child
        ns = self.next_sibling
        r = self.rect
        inf = self.info

        self._become(self.first_child)
        while True:
            yield self
            if 0 == self.next_sibling:
                break
            else:
                self._become(self.next_sibling)

        # Go back to becoming the same node we were
        self.index = idx
        self.first_child = fc
        self.next_sibling = ns
        self.rect = r
        self.info = inf


def avg_diagonals(node, onodes, memo_tab):
    nidx = node.index
    sv = 0.0
    diag = 0.0
    for onode in onodes:
        k1 = (nidx, onode.index)
        k2 = (onode.index, nidx)
        if k1 in memo_tab:
            diag = memo_tab[k1]
        elif k2 in memo_tab:
            diag = memo_tab[k2]
        else:
            diag = node.rect.union(onode.rect).diagonal()
            memo_tab[k1] = diag

        sv += diag

    return sv / len(onodes)


def silhouette_w(node, cluster, next_closest_cluster, memo):
    ndist = avg_diagonals(node, cluster, memo)
    sdist = avg_diagonals(node, next_closest_cluster, memo)
    return (sdist - ndist) / max(sdist, ndist)


def silhouette_coeff(clustering, memo_tab):
    if (len(clustering) == 1): return 1.0

    coeffs = []
    for cluster in clustering:
        others = [c for c in clustering if c is not cluster]
        others_cntr = [center_of_gravity(c) for c in others]
        ws = [silhouette_w(node, cluster, others[closest(others_cntr, node)], memo_tab) for node in cluster]
        cluster_coeff = sum(ws) / len(ws)
        coeffs.append(cluster_coeff)
    return sum(coeffs) / len(coeffs)


def center_of_gravity(nodes):
    totarea = 0.0
    xs, ys = 0, 0
    for n in nodes:
        if not (n.rect.x == 0 and n.rect.y == 0 and n.rect.xx == 0 and n.rect.yy == 0):
            x, y, w, h = n.rect.extent()
            a = w * h
            xs = xs + (a * (x + (0.5 * w)))
            ys = ys + (a * (y + (0.5 * h)))
            totarea = totarea + a
    return (xs / totarea), (ys / totarea)


def closest(centroids, node):
    x, y = center_of_gravity([node])
    dist = -1
    ridx = -1

    for (i, (xx, yy)) in enumerate(centroids):
        dsq = ((xx - x) ** 2) + ((yy - y) ** 2)
        if -1 == dist or dsq < dist:
            dist = dsq
            ridx = i
    return ridx


def k_means_cluster(root, k, nodes):
    t = time.clock()
    if len(nodes) <= k: return [[n] for n in nodes]

    ns = list(nodes)
    root.stats["count_kmeans_iter_f"] += 1

    # Initialize: take n random nodes
    random.shuffle(ns)

    cluster_starts = ns[:k]
    cluster_centers = [center_of_gravity([n]) for n in ns[:k]]

    # Loop until stable
    while True:
        root.stats["sum_kmeans_iter_f"] += 1
        clusters = [[] for c in cluster_centers]

        for n in ns:
            idx = closest(cluster_centers, n)
            clusters[idx].append(n)

        clusters = [c for c in clusters if len(c) > 0]

        for c in clusters:
            if (len(c) == 0):
                print("Error....")
                print("Nodes: %d, centers: %s" % (len(ns),
                                                  repr(cluster_centers)))

            assert (len(c) > 0)

        rest = ns
        first = False

        new_cluster_centers = [center_of_gravity(c) for c in clusters]
        if new_cluster_centers == cluster_centers:
            root.stats["avg_kmeans_iter_f"] = float(root.stats["sum_kmeans_iter_f"] / root.stats["count_kmeans_iter_f"])
            root.stats["longest_kmeans"] = max(root.stats["longest_kmeans"], (time.clock() - t))
            return clusters
        else:
            cluster_centers = new_cluster_centers
