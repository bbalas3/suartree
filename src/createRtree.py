import random
import time

from src import rect, rtree
from src.rect import Node, Descriptor

from src.utils import load, generate_random, calculate_metrics, parse_data, area, query_rtree, \
    query_sequential_intersection

__author__ = "Booma Sowkarthiga Balasubramani"

FIELDS = ['bextent', 'the_geom_poly_lu', 'GeoId', 'block_area', 'total_pop', 'tract_bloc', 'landuse', 'lu_area',
          'pop_per_polygon', 'updated_block_area', 'inhabited_noninhabited', 'pop_rate']

# DATASET FILE
DATA_SET = "data/bb_results_full.csv"


class SaurTreeExperiment:
    @staticmethod
    def run_case1(total_experiments=10000, x_bound=(-87.5509, -87.7073), y_bound=(41.7047, 41.9468)):
        """
        Execution time with uniform error threshold

        :param total_experiments: number of experiments to run
        :param x_bound: upper and lower bounds for x
        :param y_bound: upper and lower bounds for y
        :return:
        """

        Node.meta_data = {}
        response = load(DATA_SET, FIELDS)
        Node.meta_data = response.get('SAUR_TREE')

        tot_exec_time = 0
        for exp in range(total_experiments):
            x = generate_random(*x_bound)
            y = generate_random(*y_bound)
            xx = x + 0.10
            yy = y - 0.10
            error_percent = random.randint(0, 100)
            start_time = time.time()
            results = Node.query(rect.Rect(x, y, xx, yy), error_percent)
            run_time = time.time() - start_time
            tot_exec_time += run_time

        print("Avg Execution Time = {}".format(calculate_metrics(total_experiments, tot_exec_time)))

    @staticmethod
    def run_case1b(total_experiments=10000, bounds=0.025, x_bound=(-87.5509, -87.7073),
                   y_bound=(41.7047, 41.9468)):
        """
        Execution time and node info with uniformly random error threshold for a given query region size

        :param total_experiments: number of experiments to run
        :param bounds: size of the query region (in degrees)
        :param x_bound: upper and lower bounds for x
        :param y_bound: upper and lower bounds for y
        :return:
        """

        Node.meta_data = {}
        response = load(DATA_SET, FIELDS)
        Node.meta_data = response.get('SAUR_TREE')

        tot_exec_time = 0
        tot_intersecting_nodes = 0
        actual_intersecting_nodes = 0
        for exp in range(total_experiments):
            x = generate_random(*x_bound)
            y = generate_random(*y_bound)
            xx = x + bounds
            yy = y - bounds
            error_percent = random.randint(0, 100)
            start_time = time.time()
            results, tot_int_node, actual_int_node = Node.query_case1b(rect.Rect(x, y, xx, yy), error_percent)
            run_time = time.time() - start_time
            tot_exec_time += run_time
            tot_intersecting_nodes += tot_int_node
            actual_intersecting_nodes += actual_int_node

        print("Avg Execution Time = {}".format(calculate_metrics(total_experiments, tot_exec_time)))
        print("Avg no. of nodes tested = {}".format(calculate_metrics(total_experiments, tot_intersecting_nodes)))
        print("Avg no. of nodes satisfying the conditions = {}".format(
            calculate_metrics(total_experiments, actual_intersecting_nodes)))

    @staticmethod
    def run_case2(total_experiments=10000, bounds=0.025, x_bound=(-87.5509, -87.7073), y_bound=(41.7047, 41.9468)):
        """
        With descriptors and a uniformly random error threshold for a given query region size

        :param total_experiments: number of experiments to run
        :param bounds: size of the query region (in degrees)
        :param x_bound: upper and lower bounds for x
        :param y_bound: upper and lower bounds for y
        :return:
        """

        Node.meta_data = {}
        response = load(DATA_SET, FIELDS)
        Node.meta_data = response.get('SAUR_TREE')

        tot_exec_time = 0
        tot_intersecting_nodes = 0
        actual_intersecting_nodes = 0
        for exp in range(total_experiments):
            x = generate_random(*x_bound)
            y = generate_random(*y_bound)
            xx = x + bounds
            yy = y - bounds
            error_percent = random.randint(0, 100)
            start_time = time.time()
            results, tot_int_node, actual_int_node = Node.query_case2(rect.Rect(x, y, xx, yy), error_percent)
            run_time = time.time() - start_time
            tot_exec_time += run_time
            tot_intersecting_nodes += tot_int_node
            actual_intersecting_nodes += actual_int_node

        print("Avg Execution Time = {}".format(calculate_metrics(total_experiments, tot_exec_time)))
        print("Avg no. of nodes tested = {}".format(calculate_metrics(total_experiments, tot_intersecting_nodes)))
        print("Avg no. of nodes satisfying the conditions = {}".format(
            calculate_metrics(total_experiments, actual_intersecting_nodes)))

    @staticmethod
    def run_case3(total_experiments=10000, bounds=0.025, x_bound=(-87.5509, -87.7073), y_bound=(41.7047, 41.9468)):
        """
        With numerical descriptors and a uniformly random error threshold for a given query region size

        :param total_experiments: number of experiments to run
        :param bounds: size of the query region (in degrees)
        :param x_bound: upper and lower bounds for x
        :param y_bound: upper and lower bounds for y
        :return:
        """

        Node.meta_data = {}
        response = load(DATA_SET, FIELDS)
        Node.meta_data = response.get('SAUR_TREE')

        tot_exec_time = 0
        tot_intersecting_nodes = 0
        actual_intersecting_nodes = 0
        # node_res_total_pop = []
        # node_res_intersecting_area = []
        # node_res_total_area = []
        for exp in range(total_experiments):
            x = generate_random(*x_bound)
            y = generate_random(*y_bound)
            xx = x + bounds
            yy = y - bounds
            error_percent = random.randint(0, 100)
            start_time = time.time()
            results, tot_int_node, actual_int_node = Node.query_case3(rect.Rect(x, y, xx, yy), error_percent)
            run_time = time.time() - start_time
            tot_exec_time += run_time
            tot_intersecting_nodes += tot_int_node
            actual_intersecting_nodes += actual_int_node
            # for node, intersecting_area, total_area_cur in results:
            #     node_res_total_pop.append(node.info.get('desc').pop)
            #     node_res_intersecting_area.append(intersecting_area)
            #     node_res_total_area.append(total_area_cur)

        # pop_in_qr = int((sum(node_res_total_pop) / sum(node_res_total_area)) * sum(node_res_intersecting_area))
        print("Avg Execution Time = {}".format(calculate_metrics(total_experiments, tot_exec_time)))
        print("Avg no. of nodes tested = {}".format(calculate_metrics(total_experiments, tot_intersecting_nodes)))
        print("Avg no. of nodes satisfying the conditions = {}".format(
            calculate_metrics(total_experiments, actual_intersecting_nodes)))

    @staticmethod
    def run_case4(total_experiments=10000, bounds=0.025, error_percent=100, x_bound=(-87.5509, -87.7073),
                  y_bound=(41.7047, 41.9468)):
        """
        Case 4a: Error threshold = 'max' and 'min', for a given query region size
        Case 4b: Varying error thresholds for a given query region size

        :param total_experiments: number of experiments to run
        :param bounds: size of the query region (in degrees)
        :param error_percent: error threshold (alpha) in query
        :param x_bound: upper and lower bounds for x
        :param y_bound: upper and lower bounds for y
        :return:
        """

        Node.meta_data = {}
        response = load(DATA_SET, FIELDS)
        Node.meta_data = response.get('SAUR_TREE')

        tot_exec_time = 0
        tot_intersecting_nodes = 0
        actual_intersecting_nodes = 0

        # Vars defined for local testing
        # node_res_total_pop = []
        # node_res_intersecting_area = []
        # node_res_total_area = []

        for exp in range(total_experiments):
            x = generate_random(*x_bound)
            y = generate_random(*y_bound)
            xx = x + bounds
            yy = y - bounds
            start_time = time.time()
            results, tot_int_node, actual_int_node = Node.query_case3(rect.Rect(x, y, xx, yy), error_percent)
            run_time = time.time() - start_time
            tot_exec_time += run_time
            tot_intersecting_nodes += tot_int_node
            actual_intersecting_nodes += actual_int_node
            # Commented out for local testing
            # for node, intersecting_area, total_area_cur in results:
            #     node_res_total_pop.append(node.info.get('desc').pop)
            #     node_res_intersecting_area.append(intersecting_area)
            #     node_res_total_area.append(total_area_cur)

        # pop_in_qr = int((sum(node_res_total_pop) / sum(node_res_total_area)) * sum(node_res_intersecting_area))

        print("Total Avg Execution Time = {}".format(calculate_metrics(total_experiments, tot_exec_time)))
        print("Avg total no. of intersecting nodes = {}".format(
            calculate_metrics(total_experiments, tot_intersecting_nodes)))
        print("Avg actual no. of nodes accessed by the query = {}".format(
            calculate_metrics(total_experiments, actual_intersecting_nodes)))

    @staticmethod
    def run_rtree_suartree_baselines(x=-87.825, y=41.645, xx=-87.525, yy=42.019, x_bounds=(-87.525, -87.825),
                                     y_bounds=(41.645, 42.019), percentage=10, total_experiments=10000,
                                     landuse_constraint=1130.0):
        """
        Run baseline for R-tree, SUAR-tree, and sequential execution

        :param x: x of MBR in root
        :param y: y of MBR in root
        :param xx: xx of MBR in root
        :param yy: yy of MBR in root
        :param x_bounds: upper and lower bounds for x
        :param y_bounds: upper and lower bounds for y
        :param percentage: percentage of area
        :param total_experiments: number of experiments to run
        :param landuse_constraint: query constraint on landuse
        :return:
        """

        # for sequential baseline
        raw_parsed_data = parse_data(DATA_SET, FIELDS)

        # clear SUAR-tree data store
        Node.meta_data = {}

        # load the data set and build R-tree and SUAR-tree
        response = load(DATA_SET, FIELDS)

        # get rtree and saur tree instances
        rtree_instance = response.get('RTREE')
        Node.meta_data = response.get('SAUR_TREE')

        tot_exec_time_rtree = 0
        tot_exec_time_suar_tree = 0
        tot_intersecting_nodes_rtree = 0
        actual_intersecting_nodes_rtree = 0
        tot_intersecting_nodes_suar_tree = 0
        actual_intersecting_nodes_suar_tree = 0
        tot_exec_time_seq = 0
        # counter= 0
        for exp in range(total_experiments):

            # Generate a query region
            query_x, query_y, query_xx, query_yy = query_rect_generator(area(x, y, xx, yy), percentage, x_bounds,
                                                                        y_bounds)

            # Query R_tree
            r_start_time = time.time()
            results_rtree, tot_int_node_rtree, actual_int_node_rtree = query_rtree(rtree_instance,
                                                                                   rect.Rect(query_x, query_y, query_xx,
                                                                                             query_yy))
            for res in results_rtree:
                cur_landuse = []
                if isinstance(res, float):
                    cur_landuse.append(res)
                elif isinstance(res, list):
                    cur_landuse = res
                if landuse_constraint in cur_landuse:
                    actual_intersecting_nodes_rtree += 1
            r_tree_run_time = time.time() - r_start_time

            # Query SUAR_tree
            suar_start_time = time.time()
            results_suar_tree, tot_int_node_suar_tree, actual_int_node_suar_tree = Node.query_baseline(
                rect.Rect(query_x, query_y, query_xx, query_yy), landuse_constraint)
            suar_tree_run_time = time.time() - suar_start_time

            # Sequential
            query_bounds = "POLYGON ((" + str(query_x) + " " + str(query_yy) + "," \
                           + str(query_xx) + " " + str(query_yy) + "," + str(query_xx) + " " + str(query_y) + "," + str(
                query_x) + " " + str(query_y) + \
                           "," + str(query_x) + " " + str(query_yy) + "))"
            seq_start_time = time.time()

            query_sequential_intersection(raw_parsed_data, query_bounds)

            seq_run_time = time.time() - seq_start_time

            tot_exec_time_rtree += r_tree_run_time
            tot_exec_time_suar_tree += suar_tree_run_time
            tot_exec_time_seq += seq_run_time
            tot_intersecting_nodes_rtree += tot_int_node_rtree
            tot_intersecting_nodes_suar_tree += tot_int_node_suar_tree
            actual_intersecting_nodes_suar_tree += actual_int_node_suar_tree
        # Metrics
        print("Avg execution time (Rtree) = {}".format(calculate_metrics(total_experiments, tot_exec_time_rtree)))
        print("Avg execution time (Sequential) = {}".format(calculate_metrics(total_experiments, tot_exec_time_seq)))
        print(
            "Avg execution time (SUARtree) = {}".format(calculate_metrics(total_experiments, tot_exec_time_suar_tree)))
        print("Avg no. of nodes tested (Rtree) = {}".format(
            calculate_metrics(total_experiments, tot_intersecting_nodes_rtree)))
        print("Avg no. of leaves satisfying condition (Rtree) = {}".format(
            calculate_metrics(total_experiments, actual_intersecting_nodes_rtree)))
        print("Avg no. of nodes tested (SUARtree) = {}".format(
            calculate_metrics(total_experiments, tot_intersecting_nodes_suar_tree)))
        print("Avg no. of nodes satisfying condition (SUARtree) = {}".format(
            calculate_metrics(total_experiments, actual_intersecting_nodes_suar_tree)))


boundaries_used_so_far = []
landuse_constraint = random.choice(
    [6400.0, 1511.0, 1140.0, 6300.0, 1564.0, 5000.0, 6200.0, 4120.0, 1215.0, 4110.0, 1111.0, 1130.0, 3300.0, 3200.0,
     1420.0, 1433.0, 1520.0, 1561.0, 4130.0, 1350.0, 1216.0, 4140.0, 1512.0, 1240.0, 3100.0, 1112.0, 1370.0, 1321.0,
     1330.0, 3400.0, 1550.0, 1563.0,
     1565.0, 6100.0, 1431.0, 2000.0, 1432.0, 1212.0, 1562.0, 4240.0, 1310.0, 3500.0, 1151.0, 1220.0, 1360.0, 4220.0,
     1410.0, 1570.0, 1214.0, 1322.0,
     1250.0, 1450.0, 9999.0, 1540.0, 4210.0, 1211.0, 1530.0, 1340.0, 4230.0])


def query_rect_generator(total_area, percentage, x_bounds, y_bounds):
    """
    Generate random query region comprised of a given percentage of area

    :param total_area: area of the MBR of the root node
    :param percentage: percentage of area
    :param x_bounds: upper and lower bounds for x
    :param y_bounds: upper and lower bounds for y
    :return:
    """
    while True:
        x = generate_random(*x_bounds)
        xx = generate_random(*x_bounds)
        while x == xx:
            xx = generate_random(*x_bounds)
        y = generate_random(*y_bounds)
        yy = (((percentage / 100) * total_area) / (abs(x - xx))) + y
        q = [x, y, xx, yy]
        if q not in boundaries_used_so_far:
            boundaries_used_so_far.append(q)
        return x, y, xx, yy


if __name__ == '__main__':
    test_runner = SaurTreeExperiment()
    test_runner.run_rtree_suartree_baselines(x=-87.825, y=41.645, xx=-87.525, yy=42.019, x_bounds=(-87.525, -87.825),
                                             y_bounds=(41.645, 42.019), percentage=10, total_experiments=10000)
