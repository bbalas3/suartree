import math
from collections import defaultdict

__author__ = "Booma Sowkarthiga Balasubramani, Xu Teng"


class Node:
    meta_data = {}

    def __init__(self, idx):
        self.idx = idx
        self.children = []
        self.info = {}
        self.error_percent = None
        Node.meta_data[idx] = self

    def calc_error_percent(self):
        """
        Calculate error percentage

        :return:
        """
        error_percent = None
        exact = 0

        for ch in self.get_all_childrens():
            if ch.info.get('desc').area != 0:
                exact += ch.info.get('desc').pop / ch.info.get('desc').area

        approx = 0
        if self.info.get('desc').area != 0:
            approx += self.info.get('desc').pop / self.info.get('desc').area

        if exact != 0:
            error_percent = round((abs(approx - exact) / abs(exact)) * 100, 2)
        else:
            error_percent = 0
        print(self.idx, "-->", error_percent, ": ", self.info.get('rect'))
        self.error_percent = error_percent

    @staticmethod
    def update_error_percentages_for_all_nodes():
        """
        Update error percentages for all nodes

        :return:
        """
        for node in Node.meta_data.values():
            if node.children:
                node.calc_error_percent()

    @staticmethod
    def get(idx):
        """
        Retrieve the node for the given index

        :param idx:
        :return:
        """
        if idx in Node.meta_data:
            return Node.meta_data.get(idx)
        return Node(idx)

    @staticmethod
    def delete(idx):
        """
        Delete the node (and its sub-tree) for the given index

        :param idx:
        :return:
        """
        total = []
        if idx not in Node.meta_data:
            return total
        stack = [Node.get(idx)]
        while stack:
            cur = stack[-1]
            del stack[-1]

            del Node.meta_data[cur.idx]
            total.append(cur.idx)

            stack.extend(cur.children)
        return total

    def get_all_childrens(self):
        """
        Retrieve all intermediate nodes (non-leaf) of a node

        :return:
        """
        results = []
        stack = [self]
        while stack:
            cur = stack[-1]
            del stack[-1]

            if not cur.children:
                results.append(cur)
            else:
                stack.extend(cur.children)
        return results

    @staticmethod
    def query_baseline(rect, landuse_constraint):
        """
        Baseline for SUAR-tree

        :param rect: query region
        :param landuse_constraint: query constraint on landuse
        :return:
        """
        results = []
        stack = [Node.get(0)]  # starts with root
        estimated_node_access_count = 0
        actual_node_access_count = 0
        cur_landuse = []
        while stack:
            cur = stack[-1]
            del stack[-1]

            if rect.does_intersect(cur.info.get('rect')):
                estimated_node_access_count += 1
                if isinstance(cur.info.get('desc').land_use, float):
                    cur_landuse.append(cur.info.get('desc').land_use)
                elif isinstance(cur.info.get('desc').land_use, list):
                    cur_landuse = cur.info.get('desc').land_use
                if landuse_constraint in cur_landuse:
                    actual_node_access_count += 1
                    stack.extend(cur.children)
                    if not cur.children:  # Leaf node
                        results.append([cur, 0, 0])
                        # else:
                        #    stack.extend(cur.children)  # add children of intersecting node which intersects with Query rect

        return results, estimated_node_access_count, actual_node_access_count

    @staticmethod
    def query_case1b(rect, error_threshold):
        """
        Execution time and node info with uniformly random error threshold for a given query region size

        :param rect: query region
        :param error_threshold: error threshold (alpha) in query
        :return:
        """
        results = []
        stack = [Node.get(0)]  # Starts with root
        estimated_node_access_count = 0
        actual_node_access_count = 0
        while stack:
            cur = stack[-1]
            del stack[-1]

            if rect.does_intersect(cur.info.get('rect')):
                estimated_node_access_count += 1
                cur_node_error_percent = cur.error_percent
                if not cur_node_error_percent:  # cur_node_error_percent is None
                    cur_node_error_percent = 0
                if cur_node_error_percent <= error_threshold:
                    actual_node_access_count += 1
                    results.append([cur, 0, 0])
                else:
                    stack.extend(cur.children)  # Add children of intersecting node which intersects with Query rect
        return results, estimated_node_access_count, actual_node_access_count

    @staticmethod
    def query_case2(rect, error_threshold):
        """
        With Descriptors and a uniformly random error threshold for a given query region size

        :param rect: query region
        :param error_threshold: error threshold (alpha) in query
        :return:
        """
        results = []
        stack = [Node.get(0)]  # starts with root
        estimated_node_access_count = 0
        actual_node_access_count = 0
        cur_landuse = []
        while stack:
            cur = stack[-1]
            del stack[-1]

            if rect.does_intersect(cur.info.get('rect')):
                estimated_node_access_count += 1
                cur_node_error_percent = cur.error_percent
                if not cur_node_error_percent:  # cur_node_error_percent is None
                    cur_node_error_percent = 0
                if isinstance(cur.info.get('desc').land_use, float):
                    cur_landuse.append(cur.info.get('desc').land_use)
                elif isinstance(cur.info.get('desc').land_use, list):
                    cur_landuse = cur.info.get('desc').land_use
                if cur_node_error_percent <= error_threshold and 1111.0 in cur_landuse:
                    actual_node_access_count += 1
                    results.append([cur, 0, 0])
                else:
                    stack.extend(cur.children)  # add children of intersecting node which intersects with Query rect
        return results, estimated_node_access_count, actual_node_access_count

    @staticmethod
    def query_case3(rect, error_threshold):
        """
        With numerical descriptors and a uniformly random error threshold for a given query region size

        :param rect: query region
        :param error_threshold: error threshold (alpha) in query
        :return:
        """
        results = []
        stack = [Node.get(0)]  # starts with root
        estimated_node_access_count = 0
        actual_node_access_count = 0
        while stack:
            cur = stack[-1]
            del stack[-1]

            if rect.does_intersect(cur.info.get('rect')):
                estimated_node_access_count += 1
                cur_node_error_percent = cur.error_percent
                if not cur_node_error_percent:  # cur_node_error_percent is None
                    cur_node_error_percent = 0
                if isinstance(cur.info.get('desc').land_use, float):
                    cur_landuse.append(cur.info.get('desc').land_use)
                elif isinstance(cur.info.get('desc').land_use, list):
                    cur_landuse = cur.info.get('desc').land_use
                if cur_node_error_percent <= error_threshold and 1111.0 in cur_landuse:
                    actual_node_access_count += 1
                    intersecting_area = Node.calculate_intersecting_area(cur, rect)
                    total_area_cur = cur.info.get('rect').area()
                    results.append([cur, intersecting_area, total_area_cur])
                else:
                    stack.extend(cur.children)  # add children of intersecting node which intersects with query rect
        return results, estimated_node_access_count, actual_node_access_count

    @staticmethod
    def query(rect, error_threshold):
        """
        Query the tree for a given error threshold

        :param rect: query rectangle
        :param error_threshold:
        :return:
        """
        results = []
        stack = [Node.get(0)]  # starts with root
        estimated_node_access_count = 0
        actual_node_access_count = 0
        while stack:
            cur = stack[-1]
            del stack[-1]

            if rect.does_intersect(cur.info.get('rect')):
                estimated_node_access_count += 1
                cur_node_error_percent = cur.error_percent
                if not cur_node_error_percent:  # cur_node_error_percent is None
                    cur_node_error_percent = 0
                if cur_node_error_percent <= error_threshold:
                    actual_node_access_count += 1
                    intersecting_area = Node.calculate_intersecting_area(cur, rect)
                    total_area_cur = cur.info.get('rect').area()
                    results.append([cur, intersecting_area, total_area_cur])
                else:
                    stack.extend(cur.children)  # add children of intersecting node which intersects with query rect
        return results

    def calculate_intersecting_area(self, rect):
        """
        Area of intersection

        :param rect: query rectangle
        :return:
        """
        tot_area = rect.intersect(self.info.get('rect')).area()  # self.info.get('rect')
        return tot_area

    @staticmethod
    def consolidate_descriptors(descriptor_list):
        """
        Aggregate descriptors

        :param descriptor_list: list of descriptors
        :return:
        """
        keys = {}

        # if descriptor_list and isinstance(descriptor_list[-1], Descriptor):
        if descriptor_list and hasattr(descriptor_list[-1], 'area'):
            descriptor_list = [(desc.land_use, desc.area, desc.pop) for desc in descriptor_list]

        tot_area = sum([v[1] for v in descriptor_list])
        tot_pop = sum([v[2] for v in descriptor_list])

        all_land_use = []
        res = []
        for descriptor in descriptor_list:
            if isinstance(descriptor[0], list):
                all_land_use.extend(descriptor[0])
            else:
                all_land_use.append(descriptor[0])

        all_land_use = list(set(all_land_use))
        descriptor = Descriptor(all_land_use, tot_area, tot_pop)
        res.append(descriptor)

        return descriptor

    @staticmethod
    def backfill_descriptors(root):
        """
        Build SUAR-tree by using the R-tree root object

        :param root: root of the R-tree
        :return:
        """
        DESC_MAP = {}
        CHILD_PARENT_MAP = {}
        PARENT_CHILD_MAP = defaultdict(list)
        TO_BE_REFILLED = []

        root_idx = root.cursor.index
        stack = [root_idx]
        while stack:
            cur = stack[0]
            del stack[0]

            root.cursor._become(cur)

            rnode = Node.get(cur)
            rnode.info['rect'] = root.cursor.rect

            if not root.cursor.has_children():
                DESC_MAP[cur] = root.cursor.info
                continue

            for c in root.cursor.children():
                rnode.children.append(Node.get(c.index))
                PARENT_CHILD_MAP[cur].append(c.index)
                CHILD_PARENT_MAP[c.index] = cur
                TO_BE_REFILLED.append(c.index)
                stack.append(c.index)

        while True:
            if not TO_BE_REFILLED:
                break
            for x in TO_BE_REFILLED:
                if DESC_MAP.get(x):
                    TO_BE_REFILLED.remove(x)

                children = PARENT_CHILD_MAP.get(x)

                can_be_filled = True
                descriptors = []
                if not children:
                    continue
                for ch in children:
                    if ch not in DESC_MAP:
                        can_be_filled = False
                        break
                    else:
                        if isinstance(DESC_MAP.get(ch), list):
                            descriptors.extend(DESC_MAP.get(ch))
                        else:
                            descriptors.append(DESC_MAP.get(ch))
                if can_be_filled:
                    DESC_MAP[x] = Node.consolidate_descriptors(descriptors)
                    TO_BE_REFILLED.remove(x)

        for k, v in DESC_MAP.items():
            Node.get(k).info['desc'] = v

        # Update the descriptors of the root node
        temp_desc = []
        for ch in Node.get(0).children:
            if isinstance(ch.info.get('desc'), list):
                temp_desc.extend(ch.info.get('desc'))
            else:
                temp_desc.append(ch.info.get('desc'))

        Node.get(0).info['desc'] = Node.consolidate_descriptors(temp_desc)
        Node.update_error_percentages_for_all_nodes()


class Descriptor(object):
    """
    Descriptors
    """
    __slots__ = ("land_use", "area", "pop")

    def __init__(self, land_use, area, pop):
        self.land_use = land_use
        self.area = area
        self.pop = pop

    def __repr__(self):
        return "LandUse: {}, LU_Area: {},  Pop_Polygon: {}".format(self.land_use, self.area, self.pop)


class Rect(object):
    """ A rectangle class that stores: an axis aligned rectangle, and: two flags (swapped_x and swapped_y).
    (The flags are stored implicitly via swaps in the order of minx/y and maxx/y.)
    """
    __slots__ = ("x", "y", "xx", "yy", "swapped_x", "swapped_y")

    def __init__(self, minx, miny, maxx, maxy):
        self.swapped_x = (maxx < minx)
        self.swapped_y = (maxy < miny)
        self.x = minx
        self.y = miny
        self.xx = maxx
        self.yy = maxy

        if self.swapped_x: self.x, self.xx = maxx, minx
        if self.swapped_y: self.y, self.yy = maxy, miny

    def coords(self):
        return self.x, self.y, self.xx, self.yy

    def overlap(self, orect):
        return self.intersect(orect).area()

    def write_raw_coords(self, toarray, idx):
        toarray[idx] = self.x
        toarray[idx + 1] = self.y
        toarray[idx + 2] = self.xx
        toarray[idx + 3] = self.yy
        if (self.swapped_x):
            toarray[idx] = self.xx
            toarray[idx + 2] = self.x
        if (self.swapped_y):
            toarray[idx + 1] = self.yy
            toarray[idx + 3] = self.y

    def area(self):
        w = self.xx - self.x
        h = self.yy - self.y
        return w * h

    def extent(self):
        x = self.x
        y = self.y
        return (x, y, self.xx - x, self.yy - y)

    def grow(self, amt):
        a = amt * 0.5
        return Rect(self.x - a, self.y - a, self.xx + a, self.yy + a)

    def intersect(self, o):
        if self is NullRect: return NullRect
        if o is NullRect: return NullRect

        nx, ny = max(self.x, o.x), max(self.y, o.y)
        nx2, ny2 = min(self.xx, o.xx), min(self.yy, o.yy)
        w, h = nx2 - nx, ny2 - ny

        if w <= 0 or h <= 0: return NullRect

        return Rect(nx, ny, nx2, ny2)

    def does_contain(self, o):
        return self.does_containpoint((o.x, o.y)) and self.does_containpoint((o.xx, o.yy))

    def does_intersect(self, o):
        return (self.intersect(o).area() > 0)

    def does_containpoint(self, p):
        x, y = p
        return (x >= self.x and x <= self.xx and y >= self.y and y <= self.yy)

    def union(self, o):
        if o is NullRect: return Rect(self.x, self.y, self.xx, self.yy)
        if self is NullRect: return Rect(o.x, o.y, o.xx, o.yy)

        x = self.x
        y = self.y
        xx = self.xx
        yy = self.yy
        ox = o.x
        oy = o.y
        oxx = o.xx
        oyy = o.yy

        nx = x if x < ox else ox
        ny = y if y < oy else oy
        nx2 = xx if xx > oxx else oxx
        ny2 = yy if yy > oyy else oyy

        res = Rect(nx, ny, nx2, ny2)

        return res

    def union_point(self, o):
        x, y = o
        return self.union(Rect(x, y, x, y))

    def diagonal_sq(self):
        if self is NullRect: return 0
        w = self.xx - self.x
        h = self.yy - self.y
        return w * w + h * h

    def diagonal(self):
        return math.sqrt(self.diagonal_sq())


NullRect = Rect(0.0, 0.0, 0.0, 0.0)
NullRect.swapped_x = False
NullRect.swapped_y = False


def union_all(kids):
    cur = NullRect
    for k in kids: cur = cur.union(k.rect)
    assert (False == cur.swapped_x)
    return cur
